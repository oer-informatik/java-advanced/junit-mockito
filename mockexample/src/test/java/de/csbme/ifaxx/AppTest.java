package de.csbme.ifaxx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.Test;

import org.mockito.InOrder;

import org.mockito.Mockito;

import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AppTest {

        @Mock
        Sensor mockSensor;

        @Test
        void testGetInterpretedTemperature() {

                /* given: Preparation */
                App myApp = new App();
                when(mockSensor.getTemperatur()).thenReturn(12.345);
                myApp.setTempSensor(mockSensor);

                /* when: Execution */
                String result = myApp.getInterpretedTemperature();

                /* then: Verification */
                String expected = "It's ok";
                assertEquals(expected, result);
                verify(mockSensor).getTemperatur();
        }

        @Test
        void testCachedGetInterpretedTemperature() {

                /* given: Preparation */
                App myApp = new App();
                when(mockSensor.getTemperatur()).thenReturn(12.345);
                myApp.setTempSensor(mockSensor);

                /* when: Execution */
                String result = myApp.getInterpretedTemperature();
                String result2 = myApp.getInterpretedTemperature();

                /* then: Verification */
                String expected = "It's ok";
                assertEquals(expected, result);
                assertEquals(expected, result2);
                verify(mockSensor, atMost(1)).getTemperatur();
        }

        @Test
        void testMultipleGetInterpretedTemperature() {

                /* given: Preparation */
                App myApp = new App();
                when(mockSensor.getTemperatur()).thenReturn(12.345);
                myApp.setTempSensor(mockSensor);

                /* when: Execution */
                String result = myApp.getInterpretedTemperature();

                /* then: Verification */
                String expected = "It's ok";
                assertEquals(expected, result);

                InOrder inOrder = Mockito.inOrder(mockSensor, mockSensor);
                inOrder.verify(mockSensor).setOffsetTemperature(anyDouble());
                inOrder.verify(mockSensor).getTemperatur();
                verifyNoMoreInteractions(mockSensor);
        }

        @Test
        void testGetInterpreted2Temperature() {

                /* given: Preparation */
                App myApp = new App();
                when(mockSensor.getTemperatur()).thenReturn(14.0);
                myApp.setTempSensor(mockSensor);

                /* when: Execution */
                String result = myApp.getInterpretedTemperature();

                /* then: Verification */
                String expected = "It's ok";
                assertEquals(expected, result);
        }

        @Test
        void testManualStubGetInterpretedTemperature() {
                
                // given: Preparation
                App myApp = new App();
                Sensierbar stubSensor = new StubSensor();
                myApp.setTempSensor(stubSensor);

                // when: Execution
                String result = myApp.getInterpretedTemperature(); // im Stub: 13.456

                // then: Verification
                String expected = "It's ok";
                assertEquals(expected, result);
        }

}
