package de.csbme.ifaxx;

import java.util.ArrayList;

class Calculator{
    public static void mockmain( String[] args ){
        Calculator myCal = new Calculator();
        myCal.addPosition(1.50, "Apfel");
        myCal.addPosition(2.50, "Banane");
        myCal.addPosition(3.50, "Birne");
        System.out.println( "Der Gesamtpreis ist: "+myCal.sumPositions().toString() );

    //Calculator myCal = mock(Calculator.class);
    //when(myCal.sumPositions()).thenReturn(5.6);

    myCal.addPosition(1.50, "Apfel");
    }
    ArrayList<Position> myPositions = new ArrayList<>();
    public void addPosition(Position newPosition){
        myPositions.add(newPosition);
    }

    public void addPosition(Double preis, String name){
        addPosition(new Position(preis,name));
    }

    public Double sumPositions(){
        Double sum = 0.0;
        for(Position pos : myPositions){
            sum = sum +  pos.getPreis();
        }
        return sum;
    }

}

class Position{
    private Double preis;
    private String name;
    public Double getPreis(){
        return preis;
    }
    public String getName(){
        return name;
    }
    Position(Double preis, String name){
        this.preis = preis;
        this.name = name;
    }

}
