package de.csbme.ifaxx;

import java.util.Random;

public class App {
    Sensierbar tempSensor;
    Double cachedTemp;

    public static void main( String[] args ){
        App myApp = new App();
        for (int i=0; i<10; i++){
            System.out.println(myApp.getInterpretedTemperature());
        }
    }

    public String getInterpretedTemperature(){
       
       Double temp;
        if (cachedTemp != -Double.MAX_VALUE){
            temp = cachedTemp;
        }else{
            tempSensor.setOffsetTemperature(0.0);
            temp = tempSensor.getTemperatur();
            cachedTemp = temp;
        }
        String interpretedTemperature = "";
        System.out.println("Interpretierter Wert: "+temp.toString());
        if (temp<0){interpretedTemperature = "It's very cold";
        }else if (temp<10){interpretedTemperature = "It's cold";
        }else if (temp<20){interpretedTemperature = "It's ok";
        }else if (temp<30){interpretedTemperature = "It's warm";
        }else {interpretedTemperature = "It's hot";
        }
        return interpretedTemperature;
    }
    
    public App(){
        tempSensor = new Sensor();
        cachedTemp = -Double.MAX_VALUE;
    }

    public void setTempSensor(Sensierbar mySensor){
        this.tempSensor = mySensor;
    }
}

class Sensor implements Sensierbar{
    Double offsetTemperature = 0.0;

    public Double getTemperatur(){
        // normalverteilte Zufallszahl als Pseudotemperatur
        return new Random().nextGaussian(10, 10);
    }

    public void setOffsetTemperature(Double temp){
        this.offsetTemperature = temp;
    }
}  


class StubSensor implements Sensierbar{
    public Double getTemperatur(){
        return 13.45;
    } 

    public void setOffsetTemperature(Double temp){
    }   
}


interface Sensierbar{
    public abstract Double getTemperatur();
    public abstract void setOffsetTemperature(Double temp);
}