## Die Konzepte Stubs und Mocks für Unittest am Beispiel Mockito/jUnit

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/java-junit-mockito</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109393936957921303</span>

> **tl/dr;** _(ca. 20 min Lesezeit): Ohne Test-Duplikate ist es oft nicht möglich, ein einzelnes Modul zu testen. Abhängigkeiten machen sonst jeden Unit-Test zum Integrationstest. Wir lernen Mocks und Stubs kennen, und dabei - wie aus Versehen - den Unterschied zwischen zustandsbasierten und verhaltensbasierten Unittests. Am Beispiel Mockito/jUnit._

### Abhängigkeiten verhindern unabhängige Tests von Units

Nach dem V-Modell steht jedem Detaillierungsgrad, den wir in der Bearbeitung unseres Produkts haben, eine Testphase gegenüber. Auf der niedrigsten Ebene implementieren wir Module, die wir in Modultests (Unittests) testen. Im Gegensatz zu den Integrationstests der darüberliegenden Ebene sollen hierbei jedoch noch nicht das Zusammenspiel mehrerer Module getestet werden, sondern jede Einheit für sich.

![V-Modell mit den gegenüberliegenden Achsen der Erarbeitung und des Testens](images/v-modell.png)

Nicht immer ist es einfach, die einzelnen Units (Module, Klassen, Methoden - je nach Granularität der Tests) zu entkoppeln. Hierbei helfen Mocking-Frameworks. Wir wollen das an einem einfachen Beispiel durchgehen.



### Beispiel: Eine Methode testen, die von einem Sensor-Service abhängt

Ausgangslage: Wir haben eine Klasse App, in der eine Methode `getInterpretedTemperature()` enthalten ist. Diese Methode wollen wir testen, man nennt das Testobjekt auch _System under Test_ (_SUT_).

![UML Klassendiagramm der Klassen App und Sensor](plantuml/sensor-coupeled.png)

Die Methode `getInterpretedTemperature()` interpretiert die von einem Sensor gemessenen Temperaturen als "kalt", "warm", etc. und gibt beispielsweise zurück "It's cold".

```java
public String getInterpretedTemperature(){
        Sensor tempSensor = new Sensor();
        Double temp = tempSensor.getTemperatur();
        String interpretedTemperature = "";
        System.out.println("Interpretierter Wert: "+temp.toString());
        if (temp<0){interpretedTemperature = "It's very cold";
        }else if (temp<10){interpretedTemperature = "It's cold";
        }else if (temp<20){interpretedTemperature = "It's ok";
        }else if (temp<30){interpretedTemperature = "It's warm";
        }else {interpretedTemperature = "It's hot";
        }
        return interpretedTemperature;
    }
```

Anstelle einer Sensorwertermittlung per Hardware oder von einem Webservice nutzen wir für unser Beispiel als `Sensor` vereinfacht eine normalverteilte Zufallszahl:

```java
import java.util.Random;
class Sensor{
    public Double getTemperatur(){
        // normalverteilte Zufallszahl als Pseudotemperatur
        return new Random().nextGaussian(10, 10);
}   }    
```

Das Problem: bei jedem Test von `getInterpretedTemperature()` wird auch die Sensormethode  `tempSensor.getTemperatur()` aufgerufen. Wir könnten also nicht unterscheiden, ob ein Fehler im Sensor auftriff oder im eigentlichen _SUT_. Zudem kann es unerwünscht (Kundendaten), teuer (Ressourcen) oder unpraktisch (Meldungen, Mails, Ausdrucke) sein, bestimmte Dienste mit jedem Test aufzurufen. Wir müssen also einen Weg finden, dass _SUT_ zu testen, ohne den verknüpften Service aufzurufen.

#### Der manuelle Weg (ohne Mockito-Framework)

Wenn wir die Testbarkeit auf klassischem Weg herstellen wollen, müssen wir entkoppeln, Objekte extern erzeugen und diese Abhängigkeit injezieren - also den Dreischritt der _[Dependency Injection](https://oer-informatik.de/dependency-injection)_ gehen. Vorneweg einmal im Schnelldurchlauf, unten dann mit Codebeispielen:

* Ein Interface für Sensor erstellen (`Sensierbar`... jaja, ich weiß, der Name...)

* Referenzen in der App von konkret (`Sensor`) auf abstrakt (`Sensierbar`) ändern (das _Dependency Inversion Prinzip_, DIP, umsetzen)

* Objekterzeugung externalisieren bzw. Instanzübergabe per Setter ermöglichen (_Inversion of Control_, IoC).

* `Sensor` implementiert das neue Interface `Sensierbar`, daneben wird eine neue Klasse (`StubSensor`) gebildet, die nur für den Test Sensordaten bereitstellt.

* Ein Objekt von `StubSensor` während des Tests als Abhängigkeit injezieren (_Dependency Injection_, DI).

Das Ergebnis sieht im UML-Klassendiagramm dann so aus:

![Entkoppelte `Sensor`-Klasse, `StubSensor`-Klasse, die von der `AppTest`-Klasse erzeugt und in `App` injezieert wird (UML Klassendiagramm)](plantuml/sensor-decoupeled-mit-stub.png)


Codeseitig sind die Schritte die folgenden:
Das neue Interface (`Sensierbar`) muss erstellt werden:

```java
interface Sensierbar{
    public abstract Double getTemperatur();
}
```

In der Klasse `App` müssen die Referenzen auf `Sensierbar` angepasst werden und die Instanziierung ausserhalb der Methode `getInterpretedTemperature()` vorgenommen werden. Im Beispiel erreichen wir das, indem wir ein Attribut `tempSensor` mit einem öffentlichen Setter erstellen und die Sensor-Instanz im Konstruktor erzeugen:

```java
public class App {
    Sensierbar tempSensor;
    public String getInterpretedTemperature(){
       // diese Zeile entfernen: Sensierbar tempSensor = new Sensor();
       // Rest wie oben
      ...}

    public App(){
        tempSensor = new Sensor();
    }

    public void setTempSensor(Sensierbar mySensor){
        this.tempSensor = mySensor;
    }
}
```

Die Klasse Sensor muss noch erweitert werden - sie implenentiert `Sensierbar`:

```java
class Sensor implements Sensierbar{...}    
```

Zusätzlich benötigen wir noch die eigentliche Mock-Klasse: sie implementiert `Sensierbar` und gibt bei Aufruf von  `getTempertur()` immer den gleichen Wert ($13.45$) zurück:

```java
class StubSensor implements Sensierbar{
    public Double getTemperatur(){
        return 13.45;
    }    
}
```

Die Vorbereitungen wären abgeschlossen. Jetzt können wir einen jUnit-Testfall erstellen. Maven legt die Ordner unter `/test/...` die Ordnerstruktur bereits an, ggf. muss die Testklasse noch erstellt werden (in meinem Fall: `AppTest`):

```java
package de.csbme.ifaxx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class AppTest {...}
```

In der Testklasse kann dann die Testmethode unsere neue `StubSensor`-Klasse nutzen: Wir tauschen die Abhängigkeit von der `Sensor`-Klasse aus und injizieren das `StubSensor`-Objekt. Somit können wir unabhängig von `Sensor` testen:

```java
@Test
void testManualStubGetInterpretedTemperature() {
    /* given: Preparation */
    App myApp = new App();                     // Instanz der zu testenden Klasse wird erzeugt
    Sensierbar stubSensor = new StubSensor();  // Instanz des Stubsensors
    myApp.setTempSensor(stubSensor);           // Injezierung des StubSensors

    /* when: Execution */
    String result = myApp.getInterpretedTemperature();
                                              // der Wert des Stubs wird zurückgegeben: 13.45

    /* then: Verification */
    String expected = "It's ok";            //gemäß Anforderungen werden 13°C als ok gewertet
    assertEquals(expected, result);           //Aufruf der Zusicherungsmethode
}
```

Die Abhängigkeit des _System under Test_ (der App-Methode `getInterpretedTemperature();`) vom _collaborating service_ `Sensor` wurde auf diesem Weg erfolgreich umgangen - die Methode kann isoliert getestet werden.

Für das Objekt `stubSensor` haben wir festgelegt, dass es auf Aufrufe einer Methode mit einem vorgefertigten Wert antworten soll. Es beinhaltet keinerlei weitere Logik. Martin Fowler nennt diese Art Testobjekte einen _Stub_.

Die Art, wie wir kontrolliert haben, ob der Test erfolgreich verlaufen ist - nämlich in dem wir den Zustand des _SUT_ am Ende mit dem gewünschten Wert verglichen haben - nennt Martin Fowler einen _regular test_.

### Klammer auf: Stub, Mock, Dummy, Fake - ein paar Grundbegriffe

Es ist mit den Namen für Test-Doppelgänger (Mock, Stub, Fake, Dummy) wie bei "Alster" und "Radler" - für manche sind die Begriffe synonym, andere grenzen sie gegeneinander ab. Ich nutze hier die Definitionen von Martin Fowler ^[Martin Fowler bezieht sich in seinem Artikel (["Mocks Aren't Stubs", Martin Fowler 02 January 2007](https://www.martinfowler.com/articles/mocksArentStubs.html)) in der Wahl der Bezeichnungen auf ein Buch von Gerard Meszaros - das liegt mir aber nicht vor]:

- Ein **Dummy** wird nie selbst genutzt und dient nur als Platzhalter.

- Ein **Fake** enthält etwas vereinfachte Implementierung, um eine teure Operation zu simulieren (z.B. unsere Sensor-Klasse statt eines tatsächlichen Sensors oder einfache In-Memory-Listen statt Datenbankzugriffe).


- Ein **Stub-Objekt** enthalten zwar keinerlei Logik, antwortet auf Aufrufe aber mit definierten konstanten für Tests brauchbaren Rückgabewerten. In obigem Beispiel ist `StubSensor` eine Klasse für solche Objekte.

> A Test Stub is an "test-specific object that feeds the desired indirect inputs into the system under test".^[[Bei xunitpatterns findet sich eine genauere Definition eines Stub-Objekts](http://xunitpatterns.com/Test%20Stub.html)]

- Und **Mock-Objekte**? In Mock-Objekten ist neben den vorgegebenen Antworten auch noch festgelegt, welche Aufrufe dieses Objekts erwartet werden. Mit Mock-Objekten ist es also möglich, nicht nur den Zustand des _SUT_ nach dem Test zu verifizieren, sondern auch das Verhalten des _SUT_ während des Tests (durch Verifizierung der Aufrufe des Mock-Objekts).  

> _Replace an object the system under test (SUT) depends on with a test-specific object that verifies it is being used correctly by the SUT._ ^[[Bei xunitpatterns findet sich eine genauere Definition eines Mock-Objekts](http://xunitpatterns.com/Mock%20Object.html)]

Implizit haben wir soeben zwei unterschiedliche Ansätze des Unit-Testens kennengelernt:

- _Regular Tests_: Der Zustand des Testobjekts (SUT - System under Test) wird am Ende mit Assert überprüft. (Fowler nennt den Ansatz _classical_, einige bezeichnen es als die _Detroiter_ Unit-Test-Schule)

- _Behavioral Test_: Beim Testen mit Mock-Objekten wird nicht nur der Zustand des _SUT_ am Ende verglichen, sondern auch das Verhalten des _SUT_. Fowler spricht hier vom _mockist TDD_-Ansatz, der _Londoner_ Unit-Test-Schule. Hierauf baut auch das _Behavioral Driven Development_ auf.

## _Regular Tests_: Stubs mit Mockito

Wenn wir das Testframework Mockito nutzen, können wir uns einen guten Teil der Arbeit sparen: wir müssen kein Interface mehr erstellen und keine `Stubklasse` implementieren. Das `Stubobjekt` wird direkt durch das Framework per

```java
Sensor mockSensor = mock(Sensor.class);
```

erstellt und Verhalten und die vorgeschriebene Antwort per


```java
when(mockSensor.getTemperatur()).thenReturn(12.345);
```

injeziert. `mock` und `when` sind dabei statische Methoden der Klasse `Mockito` und müssen importiert werden.

```java
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
```
Der Rückgabewert von `when` ist vom Typ `OngoingStubbing`. Die Methoden, die gegen dieses Interface implementiert werden, sind wiederum vom Typ `OngoingStubbing`, weshalb sie verkettet werden können ("_Fluent API_"). Es könnte also z.B. festgelegt werden, dass beim ersten Aufruf 1, beim  zweiten 2, beim dritten 4 und beim vierten eine Exception vom Stub zurückgegeben wird:

```java
when(mockSensor.getTemperatur())
               .thenReturn(1.0)
               .thenReturn(2.0)
               .thenReturn(4.0);
               .thenThrow(new NullPointerException());
```

Das UML-Klassendiagramm sieht für den uns bislang bekannten Teil der Klasse `Mockito` und des Interfaces `OngoingStubbing` etwa so aus:

![](plantuml/mockito-class-regular-test.png)

Bis auf diese Anpassungen bleibt der Rest der Testmethode wie beim manuellen Beispiel oben:

```java
 @Test
 void testGetInterpretedTemperature() {
         /* given: Preparation */
         App myApp = new App();
         Sensor mockSensor = mock(Sensor.class);
         when(mockSensor.getTemperatur()).thenReturn(12.345); // Use mock as stub
         myApp.setTempSensor(mockSensor);

         /* when: Execution */
         String result = myApp.getInterpretedTemperature();

         /* then: Verification */
         String expected = "It's ok";
         assertEquals(expected, result);
}
```

Im Code schön zu sehen: in der _Verification_-Phase des Tests betrachten wir nur den Zustand des _SUT_. Das wollen wir im folgenden erweitern.

### _Behavioral Tests_ mit Mockito

Jetzt zünden wir die zweite Stufe: wir haben bei der Mock/Stub-Definition von Martin Fowler gelernt, dass _Mock-Objekte_ erlauben, nicht nur den Zustand des _SUT_, sondern auch dessen Verhalten zu beobachten.

Um das einmal beispielhaft auszuführen fügen wir eine Anforderung an unsere _SUT_ Methode an: Sensorergebnisse sollen während der Laufzeit zwischengespeichert werden. Ein Aufruf von `myApp.getInterpretedTemperature()` soll nur dann den Sensor abfragen, wenn kein zwischengespeicherter Wert vorliegt.

Als kleiner Zwischenschritt: Mit Hilfe der Methode `verify()` können wir im Rahmen der Verifizierungsphase im Test prüfen, ob die Methode des Mock-Objekts im Rahmen des Tests wirklich aufgerufen wurde (das einfachste Verhalten). Dazu müssen wir nur am Ende des Tests die folgende Zeile anfügen:

```java
 verify(mockSensor).getTemperatur();
```

Wenn jetzt ein böswilliger Entwickler die Logik unserer Methode `myApp.getInterpretedTemperature()` umbaut, die  Sensorabfrage auskommentiert und immer 15.0 zurückgibt:

```java
   Double temp = 15.0; //tempSensor.getTemperatur();
```

Werden alle zustandsbasierten Tests weiterhin bestanden. Die `verify()`-Zeile oben wird unseren verhaltensorientierten Test aber scheitern lassen: die Methode wird nie aufgerufen. Ein erster Schritt. Aber wie bekommen wir heraus, wie oft unser Mock aufgerufen wurde? Wir wollen ja ab dem ersten Aufruf einen gecachten Wert nutzen.

Um den Cache zu implementieren ändern wir unser Klasse `App`: ein neues Attribut `cachedTemp`, dass im Konstruktor auf einen Defaultwert gesetzt wird und in unserer `SUT`-Methode genutzt/befüllt wird:

```java
public class App {
    ...
    Double cachedTemp;
    ...
    public String getInterpretedTemperature(){
       Double temp;
        if (cachedTemp != -Double.MAX_VALUE){
            temp = cachedTemp;
        }else{
            temp = tempSensor.getTemperatur();
            cachedTemp = temp;
        }
        ...
    }

    public App(){
        tempSensor = new Sensor();
        cachedTemp = -Double.MAX_VALUE;
    }
    ...
}
```

Mockito bietet neben `verify()` auch noch weitere Möglichkeiten, das Verhalten zu überprüfen, es gibt eine Reihe weiterer statischer Methoden. In unserem Fall ist die überladene Methode `verify(mock, VerificationMode)` interessant.

![UML-Diagramm von Mockito inkl. der Verhaltenstest-Methoden verify()](plantuml/mockito-class-behavior-test.png)

Wir können über den zweiten Parameter übergeben, wie häufig eine Methode unseres Mock-Objekts aufgerufen werden soll. Wie das im einzelnen aussieht, dazu später. Jetzt wollen wir erstmal die Testmethode erstellen:

Wir rufen zweimal hintereinander unsere _SUT_-Methode auf. Als Zustand erwarten wir (in der _Verification_-Phase), dass alle den gleichen Wert "It's cold" zurück liefern. Als Verhalten erwarten wir (`verify()`), dass die Mock-Methode höchstens einmal (`atMost(1)`) aufgerufen wird:

```java
   @Test
   void testCachedGetInterpretedTemperature() {
           /* given: Preparation */
           App myApp = new App();
           Sensor mockSensor = mock(Sensor.class);
           when(mockSensor.getTemperatur()).thenReturn(12.345); // Use mock as stub
           myApp.setTempSensor(mockSensor);

           /* when: Execution */
           String result = myApp.getInterpretedTemperature();
           String result2 = myApp.getInterpretedTemperature();

           /* then: Verification */
           String expected = "It's ok";
           assertEquals(expected, result);
           assertEquals(expected, result2);
           verify(mockSensor, atMost(1)).getTemperatur();
   }
  ```

Die genutzten Bedingungen an das Verhalten des Mock-Objekts werden durch Implementierungen des Interfaces `VerificationMode` überprüft. Die Klasse `Mockito` nutzt solche Implementierungen als Parameter der Methode `verify()` und erzeugt sie auch selbst in den statischen Factory-Methoden wie `atMost()`. Die `Mockito`-Klasse ist selbst in dem folgenden Beispiel schon unübersichtlich. Trotzdem lohnt sich ein Blick auch in das zugehörige _Javadoc-File: [https://javadoc.io/doc/org.mockito/mockito-all/latest/index.html](https://javadoc.io/doc/org.mockito/mockito-all/latest/index.html)
![UML-Diagramm von Mockito inkl. der Verhaltenstest-Methoden verify()](plantuml/mockito-class-behavior-test-verify.png)

Mit Hilfe dieses Tests können wir nun überprüfen, ob das Caching funktioniert.

### Weitere Funktionalität von Mockito

Wir haben bislang nur einen `verify()` Aufruf genutzt. Wenn wir unterschiedliche Methodenaufrufe unseres Mock-Objekts überprüfen wollen, können wir diese einfach nacheinander aufführen. Wir erweitern unseren Sensor-Service (und dessen Interface) um eine zweite Methode: ein Attribut `offsetTemperature` mitsamt Setter:

 ```java
class Sensor implements Sensierbar{
    Double offsetTemperature = 0.0;

    public void setOffsetTemperature(Double temp){
        this.offsetTemperature = temp;
    }

    public Double getTemperatur(){...}
}
```

Dieser Setter wird vor jeder Messung im _SUT_ (`getInterpretedTemperature()`) auf $0.0$ gesetzt:

```java
public String getInterpretedTemperature(){
        ...
            tempSensor.setOffsetTemperature(0.0);
            temp = tempSensor.getTemperatur();
        ...}
```

In der Testmethode prüfen wir in der _Verification_-Phase:

* ob der Setter (überhaupt) mit einem beliebigen `Double`-Wert (`anyDouble()`) aufgerufen wurde,

* ob der .getTemperatur(); (überhaupt) aufgerufen wurde,

* ob alle Aufrufe des Mockobjekts verifiziert wurden, oder ob es über die vorgenannten hinaus noch weitere Mockmethoden-Aufrufe gab.

 ```java
 verify(mockSensor).setOffsetTemperature(anyDouble());
 verify(mockSensor).getTemperatur();
 verifyNoMoreInteractions(mockSensor);
 ```

Die oberen beiden Zeilen können getauscht werden, es wird also intern nur "abgehakt", welche Methoden aufgerufen wurden. Am Ende wird geschaut, ob alle Methodenaufrufe einen imaginären Haken haben. Falls Methoden mehrfach aufgerufen werden, muss dies mit den oben gezeigten `VerificationModes` angezeigt werden.

Was man an diesem Beispiel auch sieht: `anyDouble()` dient als `ArgumentMatcher` - ein komfortabler Weg, wenn man nicht präzise sagen kann, welche Argumente an das Mock-Objekt übergeben werden. Es gibt davon einige - das betroffene [Javadoc zu `ArgumentMatchers`](https://javadoc.io/static/org.mockito/mockito-core/4.9.0/org/mockito/ArgumentMatchers.html) hilft hier gerne.

Bei dem obigen Beispiel spielte die Reihenfolge, in der die `verify()`-Aufrufe erfolgten keine Rolle, es wurde nur "abgehakt", was aufgerufen wurde. Wenn auch die Aufrufreihenfolge relevant ist, kommt mit der `InOrder`-Klasse ein weiteres Feature von Mockito ins Spiel: Folgende Verifikation würde fehlschlagen, da der Aufruf im _SUT_ in anderer Reihenfolge erfolgt:

```java
InOrder inOrder = Mockito.inOrder(mockSensor, mockSensor);          
inOrder.verify(mockSensor).getTemperatur();
inOrder.verify(mockSensor).setOffsetTemperature(anyDouble());  
verifyNoMoreInteractions(mockSensor);
```

Die Instanziierung der Mock-Objekte, die wir oben in jedem Test mit der Zeile `Sensor mockSensor = mock(Sensor.class);` ausgeführt haben, kann Mockito auch automatisch erstellen für alle Attribute in der Testklasse, die mit `@Mock` annotiert sind.

```java
@Mock
Sensor mockSensor;
```

Damit der Testrunner von jUnit, der die eigentlichen Tests ausführt, diese annotierten Attribute auch findet, muss die Testklasse wiederum auch annotiert werden mit: `@ExtendWith(MockitoExtension.class)`.

Ein Beispiel mit den nötigen Imports, der auskommentierten alten Instanziierung und dem Mock-Objekt als Attribut sieht gekürzt so aus:

```java
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AppTest {

     @Mock
     Sensor mockSensor;

     @Test
     void testGetInterpretedTemperature() {
             /* given: Preparation */
             App myApp = new App();
             //Sensor mockSensor = mock(Sensor.class);
             ...
}    }    
```

Mockito bietet noch unglaublich viele weitere Möglichkeiten wie zum Umgang mit _Exceptions_, die Verifizierung regulärer Objekte mit _Spy_ usw. - ein Blick in die Framework-[Javadoc-Seite von Mockito](https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html) umreist die Möglichkeiten dieses Test-Tools.

### Fazit

Wir haben bislang nur sehr an der Oberfläche von Mockito gekratzt, aber dabei zwei wesentliche Konzepte kennengelernt:

* Das zustandsbasierte Testen mit Hilfe von _Stubs_ auf der einen und

* das verhaltensbasierte Testen mit _Mocks_ auf der anderen Seite.

Sehr häufig wird Testen nur auf die erste Technik beschränkt und damit die Möglichkeiten verspielt, die ein Mocking-Framework wie Mockito überhaupt erst bietet.

Es lohnt sich, an einem Beispiel die jeweiligen Möglichkeiten dieser zwei Arten zu testen auszuloten - und sie fortan gezielt und passend einzusetzen.

### Links und weitere Informationen

- [Das Javadoc für Mockito](https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html)

- [Mocks aren't Stubs von Martin Fowler](https://www.martinfowler.com/articles/mocksArentStubs.html)

- Alte, aber sehr lesenswerte Seite mit Hintergründen zu Unittest, Mock, Stubs usw.: [xunitpatterns.com](http://xunitpatterns.com/Test%20Double%20Patterns.html)

- Gutes Tutorial [von Vogella zu iUnit/Mockito](https://www.vogella.com/tutorials/Mockito/article.html)

- Gutes Tutorial von [Dzone zu Mockito/jUnit](https://dzone.com/refcardz/mockito?chapter=3)
 